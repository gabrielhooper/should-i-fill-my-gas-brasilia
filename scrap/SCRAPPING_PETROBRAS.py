#!/usr/bin/env python
# coding: utf-8

# In[1]:


import requests
from bs4 import BeautifulSoup
import time  
import mysql.connector as mysql
from keys import mysqlkey


# In[2]:


agora = time.strftime('%Y-%m-%d %H:%M:%S')
try:
    r = requests.get('http://www.petrobras.com.br/pt/produtos-e-servicos/precos-de-venda-as-distribuidoras/gasolina-e-diesel/')
except:
    with open('./log.txt','a') as f:
        f.write(f"{agora}\tErro na requisição\n")
    exit()


# In[20]:


html = BeautifulSoup(r.content)


# In[21]:


samples = html.findAll("tr")


# In[14]:


try:
    db = mysql.connect(
        host = "localhost",
        user = "root",
        passwd = mysqlkey,
        database = "gasolina"
    )
    cr = db.cursor()
except:
    with open('./log.txt','a') as f:
        f.write(f"{agora}\tErro na conexao com DB\n{str(samples)}\n")
    exit()


# In[22]:


try:
    cr.execute(f"SELECT count(*) FROM gasolina.preco_distribuidoras where data_criacao > '{agora[:10]}'")
    contador = cr.fetchall()
    if contador[0][0]>0:
        raise Exception()
except:
    with open('./log.txt','a') as f:
        f.write(f"{agora}\tJá houve inserção hoje\n")
    exit()


# In[17]:


try:
    for i,tr in enumerate(samples):
        if i ==0: #Ignora cabeçalho
            continue
        #print(tr)
        tds = tr.find_all("td")
        local = tds[0].string.replace("\t",'').replace("\n","")
        gasolina_a = tds[1].string.replace("\t",'').replace("\n","").replace(",",".")
        diesel_500 = tds[2].string.replace("\t",'').replace("\n","").replace(",",".")
        diesel_s10 = tds[3].string.replace("\t",'').replace("\n","").replace(",",".")
        #print(f'cidade {cidade} a gasolina vale R${gasolina_a}, o diesel 500 R${diesel_500} e o diesel s10 R${diesel_s10}\n')
        colunas = "(data_criacao,local,gasolina_a,diesel_s500,diesel_s10)";
        valores = f'("{agora}","{local}","{gasolina_a}","{diesel_500}","{diesel_s10}")'
        #print(colunas)
        #print(valores)
        #print(f"INSERT INTO preco_distribuidoras {colunas} VALUES {valores};")
        cr.execute(f"INSERT INTO preco_distribuidoras {colunas} VALUES {valores};")
    db.commit()
except:
    with open('./log.txt','a') as f:
        f.write(f"{agora}\tErro na extração e inserção\n{str(samples)}\n")
    exit()


# In[ ]:




