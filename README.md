# should-i-fill-my-gas-brasilia

Project to predict if I should fill my car tank with gas today or not (in referrence of next week gas price) based on the historical data

Exemplo de análise realizada:
Preço das distribuidoras e nas bombas possuem time-shift
![alt text](https://gitlab.com/gabrielhooper/should-i-fill-my-gas-brasilia/raw/master/imgs/postosVSdistribuidora.png "Preço das distribuidoras e nas bombas possuem time-shift")


Dados obtidos em: 
    - https://www.investing.com/currencies/usd-brl-historical-data
    - https://www.investing.com/commodities/brent-oil-historical-data
    - https://www.investing.com/commodities/crude-oil-historical-data
    - https://www.investing.com/indices/ibovespa-futures-historical-data
    - e-sic do CGU após pedido protocolo 99909.002094/2019-11    
    
    - http://dados.gov.br/dataset/serie-historica-de-precos-de-combustiveis-por-revenda
    
    
A fazer:
    - Utilizar os dados do etanol (compõe o preço do combustível)
    - Obter dados melhores dos preços da gasolina e etanol
    - Obter dados históricos do preço na refinaria (http://www.petrobras.com.br/pt/produtos-e-servicos/precos-de-venda-as-distribuidoras/gasolina-e-diesel/) pedido 99909.002094/2019-11
